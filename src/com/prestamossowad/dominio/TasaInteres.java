package com.prestamossowad.dominio;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

public class TasaInteres {

    private int id;
    private BigDecimal tea;
    private BigDecimal tim;
    private Date fechaRegistro;
    private boolean estado;
    private TipoInteres objTipoInteres;

    public TasaInteres() {
        this.id = 0;
        this.tea = new BigDecimal(BigInteger.ZERO);
        this.tim = new BigDecimal(BigInteger.ZERO);
        this.fechaRegistro = new Date();
        this.estado = false;
        this.objTipoInteres = null;
    }

    public TasaInteres(int id, BigDecimal tea, BigDecimal tim, Date fechaRegistro, boolean estado, TipoInteres objTipoInteres) {
        this.id = id;
        this.tea = tea;
        this.tim = tim;
        this.fechaRegistro = fechaRegistro;
        this.estado = estado;
       this.objTipoInteres = objTipoInteres;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public BigDecimal getTea() {
        return tea;
    }

    public void setTea(BigDecimal tea) {
        this.tea = tea;
    }

    public BigDecimal getTim() {
        return tim;
    }

    public void setTim(BigDecimal tim) {
        this.tim = tim;
    }

    public Date getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(Date fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public boolean isEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }

    public TipoInteres getObjTipoInteres() {
        return objTipoInteres;
    }

    public void setObjTipoInteres(TipoInteres objTipoInteres) {
        this.objTipoInteres = objTipoInteres;
    }
}