package com.prestamossowad.dominio;

import java.util.Date;

public class Comprobante {
    
    private int id;
    private String codigo;
    private Date fechaRegistro;
    private Pago objPago;

    public Comprobante() {
        this.id = 0;
        this.codigo = "";
        this.fechaRegistro = new Date();
    }

    public Comprobante(int id, String codigo, Date fechaRegistro, Pago objPago) {
        this.id = id;
        this.codigo = codigo;
        this.fechaRegistro = fechaRegistro;
        this.objPago = objPago;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public Date getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(Date fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public Pago getObjPago() {
        return objPago;
    }

    public void setObjPago(Pago objPago) {
        this.objPago = objPago;
    }
}