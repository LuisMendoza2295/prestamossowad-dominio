package com.prestamossowad.dominio;

import java.util.Date;

import com.prestamossowad.dominio.util.DateUtil;

public class SolicitudEliminacionPrestamo {
    private int id;
    private boolean aceptadoPrestamista;
    private boolean aceptadoPrestatario;
    private Date fechaRegistro;
    private Date fechaModificacion;
    private Usuario objUsuario;
    private Prestamo objPrestamo;

    public SolicitudEliminacionPrestamo() {
        this.id = 0;
        this.aceptadoPrestamista = false;
        this.aceptadoPrestatario = false;
        this.fechaRegistro = new Date();
        this.fechaModificacion = new Date();
        this.objUsuario = new Usuario();
        this.objPrestamo = new Prestamo();
    }

    public SolicitudEliminacionPrestamo(int id, boolean aceptadoPrestamista, boolean aceptadoPrestatario, Date fechaRegistro, Date fechaModificacion, Usuario objUsuario, Prestamo objPrestamo) {
        this.id = id;
        this.aceptadoPrestamista = aceptadoPrestamista;
        this.aceptadoPrestatario = aceptadoPrestatario;
        this.fechaRegistro = fechaRegistro;
        this.fechaModificacion = fechaModificacion;
        this.objUsuario = objUsuario;
        this.objPrestamo = objPrestamo;
        this.fechaModificacion = DateUtil.getFecha(this.fechaModificacion);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isAceptadoPrestamista() {
        return aceptadoPrestamista;
    }

    public void setAceptadoPrestamista(boolean aceptadoPrestamista) {
        this.aceptadoPrestamista = aceptadoPrestamista;
    }

    public boolean isAceptadoPrestatario() {
        return aceptadoPrestatario;
    }

    public void setAceptadoPrestatario(boolean aceptadoPrestatario) {
        this.aceptadoPrestatario = aceptadoPrestatario;
    }

    public Date getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(Date fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public Date getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaModificacion(Date fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    public Usuario getObjUsuario() {
        return objUsuario;
    }

    public void setObjUsuario(Usuario objUsuario) {
        this.objUsuario = objUsuario;
    }

    public Prestamo getObjPrestamo() {
        return objPrestamo;
    }

    public void setObjPrestamo(Prestamo objPrestamo) {
        this.objPrestamo = objPrestamo;
    }
}