package com.prestamossowad.dominio;

import java.util.Date;

import com.prestamossowad.dominio.util.DateUtil;

public class TipoInteres {

    private int id;
    private String nombre;
    private int diasPlazos;
    private boolean estado;
    private Date fechaRegistro;
    private Date fechaModificacion;
    //private TasaInteres objTasaInteres;

    public TipoInteres() {
        this.id = 0;
        this.nombre = "";
        this.diasPlazos = 0;
        this.estado = false;
        this.fechaRegistro = new Date();
        this.fechaModificacion = new Date();
        //this.objTasaInteres = new TasaInteres();
    }

    public TipoInteres(int id, String nombre, int diasPlazos, boolean estado, Date fechaRegistro, Date fechaModificacion) {
        this.id = id;
        this.nombre = nombre;
        this.diasPlazos = diasPlazos;
        this.estado = estado;
        this.fechaRegistro = fechaRegistro;
        this.fechaModificacion = fechaModificacion;
        //this.objTasaInteres = objTasaInteres;
        this.fechaModificacion = DateUtil.getFecha(this.fechaModificacion);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getDiasPlazos() {
        return diasPlazos;
    }

    public void setDiasPlazos(int diasPlazos) {
        this.diasPlazos = diasPlazos;
    }

    public boolean isEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }

    public Date getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(Date fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public Date getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaModificacion(Date fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }
    
    /*public TasaInteres getObjTasaInteres(){
    	return objTasaInteres;
    }
    
    public void setObjTasaInteres(TasaInteres objTasaInteres){
    	this.objTasaInteres = objTasaInteres;
    }*/

    /*public TasaInteres[] getLstTasasInteres() {
        return lstTasasInteres;
    }

    public void addTasaInteres(TasaInteres objTasaInteres){
		this.lstTasasInteres = Arrays.copyOf(this.lstTasasInteres, this.lstTasasInteres.length + 1);
		this.lstTasasInteres[this.lstTasasInteres.length - 1] = objTasaInteres;
	}
	
	public void setLstTasasInteres(List<TasaInteres> lstTasasInteres){
		for(TasaInteres objTasaInteres : lstTasasInteres){
			this.addTasaInteres(objTasaInteres);
		}
	}*/
}