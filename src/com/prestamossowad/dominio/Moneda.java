package com.prestamossowad.dominio;

import java.math.BigDecimal;
import java.math.BigInteger;

public class Moneda {
    private int id;
    private String nombre;
    private String abreviado;
    private BigDecimal tipoCambio;

    public Moneda() {
        this.id = 0;
        this.nombre = "";
        this.abreviado = "";
        this.tipoCambio = new BigDecimal(BigInteger.ZERO);
    }

    public Moneda(int id, String nombre, String abreviado, BigDecimal tipoCambio) {
        this.id = id;
        this.nombre = nombre;
        this.abreviado = abreviado;
        this.tipoCambio = tipoCambio;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getAbreviado() {
        return abreviado;
    }

    public void setAbreviado(String abreviado) {
        this.abreviado = abreviado;
    }

    public BigDecimal getTipoCambio() {
        return tipoCambio;
    }

    public void setTipoCambio(BigDecimal tipoCambio) {
        this.tipoCambio = tipoCambio;
    }
}