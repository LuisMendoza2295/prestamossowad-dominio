package com.prestamossowad.dominio.util;

import java.math.BigDecimal;

public class BigDecimalUtil {

	private BigDecimalUtil(){
	}
	
	public static BigDecimal round(BigDecimal number, int decimals){
        return number.divide(BigDecimal.ONE, decimals, BigDecimal.ROUND_HALF_UP);
    }
}
