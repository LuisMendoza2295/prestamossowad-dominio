package com.prestamossowad.dominio.util;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class DateUtil {

	private DateUtil(){
	}
	
	public static Date getFecha(Date fecha){
		if(fecha == null){
			Calendar calendar = new GregorianCalendar();
			calendar.setTime(new Date());
			calendar.add(Calendar.DAY_OF_MONTH, 1);
			return calendar.getTime();
		}
		
		return fecha;
	}
	
	public static int getDaysDifference(Date startDate, Date finishDate) {
        if (startDate.before(finishDate)) {
            Long diferencia = (finishDate.getTime() - startDate.getTime()) / (24 * 60 * 60 * 1000);

            return diferencia.intValue();
        } else {
            return 0;
        }
    }
}
