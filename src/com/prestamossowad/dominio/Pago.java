package com.prestamossowad.dominio;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

public class Pago {
    
    private int id;
    private BigDecimal monto;
    private boolean aprobado;
    private Date fecha;
    private Cuota objCuota;
    private TipoPago objTipoPago;

    public Pago() {
        this.id = 0;
        this.monto = new BigDecimal(BigInteger.ZERO);
        this.aprobado = false;
        this.fecha = new Date();
        this.objCuota = new Cuota();
        this.objTipoPago = new TipoPago();
    }

    public Pago(int id, BigDecimal monto, boolean aprobado, Date fecha, Cuota objCuota, TipoPago objTipoPago) {
        this.id = id;
        this.monto = monto;
        this.aprobado = aprobado;
        this.fecha = fecha;
        this.objCuota = objCuota;
        this.objTipoPago = objTipoPago;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public BigDecimal getMonto() {
        return monto;
    }

    public void setMonto(BigDecimal monto) {
        this.monto = monto;
    }

    public boolean isAprobado() {
		return aprobado;
	}

	public void setAprobado(boolean aprobado) {
		this.aprobado = aprobado;
	}

	public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Cuota getObjCuota() {
        return objCuota;
    }

    public void setObjCuota(Cuota objCuota) {
        this.objCuota = objCuota;
    }

    public TipoPago getObjTipoPago() {
        return objTipoPago;
    }

    public void setObjTipoPago(TipoPago objTipoPago) {
        this.objTipoPago = objTipoPago;
    }
}