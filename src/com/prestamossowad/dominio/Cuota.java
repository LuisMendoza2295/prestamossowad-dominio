package com.prestamossowad.dominio;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

import com.prestamossowad.dominio.util.DateUtil;

public class Cuota {

    private int id;
    private BigDecimal amortizacion;
    private BigDecimal interes;
    private BigDecimal monto;
    private BigDecimal mora;
    private Date fechaInicio;
    private Date fechaFin;
    private Date fechaRegistro;
    private Date fechaModificacion;
    private boolean pagado;

    public Cuota() {
        this.id = 0;
        this.amortizacion = new BigDecimal(BigInteger.ZERO);
        this.interes = new BigDecimal(BigInteger.ZERO);
        this.monto = new BigDecimal(BigInteger.ZERO);
        this.mora = new BigDecimal(BigInteger.ZERO);
        this.fechaInicio = new Date();
        this.fechaFin = new Date();
        this.fechaRegistro = new Date();
        this.fechaModificacion = new Date();
        this.pagado = false;
    }

    public Cuota(int id, BigDecimal amortizacion, BigDecimal interes, BigDecimal monto, BigDecimal mora, Date fechaInicio, Date fechaFin, Date fechaRegistro, Date fechaModificacion, boolean pagado) {
        this.id = id;
        this.amortizacion = amortizacion;
        this.interes = interes;
        this.monto = monto;
        this.mora = mora;
        this.fechaInicio = fechaInicio;
        this.fechaFin = fechaFin;
        this.fechaRegistro = fechaRegistro;
        this.fechaModificacion = fechaModificacion;
        this.pagado = pagado;
        this.fechaModificacion = DateUtil.getFecha(this.fechaModificacion);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public BigDecimal getAmortizacion() {
        return amortizacion;
    }

    public void setAmortizacion(BigDecimal amortizacion) {
        this.amortizacion = amortizacion;
    }

    public BigDecimal getInteres() {
        return interes;
    }

    public void setInteres(BigDecimal interes) {
        this.interes = interes;
    }

    public BigDecimal getMonto() {
        return monto;
    }

    public void setMonto(BigDecimal monto) {
        this.monto = monto;
    }

    public BigDecimal getMora() {
        return mora;
    }

    public void setMora(BigDecimal mora) {
        this.mora = mora;
    }

    public Date getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public Date getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }

    public Date getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(Date fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public Date getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaModificacion(Date fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    public boolean isPagado() {
        return pagado;
    }

    public void setPagado(boolean pagado) {
        this.pagado = pagado;
    }
    
    public BigDecimal calcularMora(){
    	BigDecimal mora = new BigDecimal(BigInteger.ZERO);
    	
    	try{
    		if(this.fechaFin.compareTo(new Date()) >= 0){
    			int diasMora = DateUtil.getDaysDifference(this.fechaFin, new Date());
    			
    			mora = this.monto.multiply(BigDecimal.valueOf(diasMora));
    		}
    	}catch(Exception e){
    		e.printStackTrace();
    	}
    	
    	return mora;
    }
    
    public BigDecimal calcularMontoPago(){
    	return this.monto.add(this.calcularMora());
    }
    
    public boolean isVencido(){
    	return true;
    }
}