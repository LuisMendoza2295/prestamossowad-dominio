package com.prestamossowad.dominio;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import com.prestamossowad.dominio.util.DateUtil;

public class Usuario {
    
    private int id; 
    private String dni;
    private String nombre;
    private String apellidoPaterno;
    private String apellidoMaterno;    
    private String username;
    private String password;    
    private Date fechaRegistro;
    private Date fechaModificacion;
    private boolean estado;    
    private TipoInteres[] lstTiposInteres;

    public Usuario() {
        this.id = 0;
        this.dni = "";
        this.nombre = "";
        this.apellidoPaterno = "";
        this.apellidoMaterno = "";
        this.username = "";
        this.password = "";
        this.fechaRegistro = new Date();
        this.fechaModificacion = new Date();
        this.estado = false;
        this.lstTiposInteres = new TipoInteres[0];
    }

    public Usuario(int id, String dni, String nombre, String apellidoPaterno, String apellidoMaterno, String username, String password, Date fechaRegistro, Date fechaModificacion, boolean estado) {
        this.id = id;
        this.dni = dni;
        this.nombre = nombre;
        this.apellidoPaterno = apellidoPaterno;
        this.apellidoMaterno = apellidoMaterno;
        this.username = username;
        this.password = password;
        this.fechaRegistro = fechaRegistro;
        this.fechaModificacion = fechaModificacion;
        this.estado = estado;
        this.lstTiposInteres = new TipoInteres[0];
        this.fechaModificacion = DateUtil.getFecha(this.fechaModificacion);
    }

    public Usuario(int id, String dni, String nombre, String apellidoPaterno, String apellidoMaterno, String username, String password, Date fechaRegistro, Date fechaModificacion, boolean estado, List<TipoInteres> lstTiposInteres) {
        this.id = id;
        this.dni = dni;
        this.nombre = nombre;
        this.apellidoPaterno = apellidoPaterno;
        this.apellidoMaterno = apellidoMaterno;
        this.username = username;
        this.password = password;
        this.fechaRegistro = fechaRegistro;
        this.fechaModificacion = fechaModificacion;
        this.estado = estado;
        this.lstTiposInteres = new TipoInteres[0];
        this.setLstTiposInteres(lstTiposInteres);
        this.fechaModificacion = DateUtil.getFecha(this.fechaModificacion);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidoPaterno() {
        return apellidoPaterno;
    }

    public void setApellidoPaterno(String apellidoPaterno) {
        this.apellidoPaterno = apellidoPaterno;
    }

    public String getApellidoMaterno() {
        return apellidoMaterno;
    }

    public void setApellidoMaterno(String apellidoMaterno) {
        this.apellidoMaterno = apellidoMaterno;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Date getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(Date fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public Date getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaModificacion(Date fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    public boolean isEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }

    public TipoInteres[] getLstTiposInteres() {
        return lstTiposInteres;
    }
    
    public void addTipoInteres(TipoInteres objTipoInteres){
    	this.lstTiposInteres = Arrays.copyOf(this.lstTiposInteres, this.lstTiposInteres.length + 1);
    	this.lstTiposInteres[this.lstTiposInteres.length - 1] = objTipoInteres;
    }
    
    public void setLstTiposInteres(List<TipoInteres> lstTiposInteres){
    	this.lstTiposInteres = new TipoInteres[0];
    	for(TipoInteres objTipoInteres : lstTiposInteres){
    		this.addTipoInteres(objTipoInteres);
    	}
    }
    
    public TipoInteres getTipoInteres(int id){
    	for(TipoInteres objTipoInteres : this.lstTiposInteres){
    		if(objTipoInteres.getId() == id){
    			return objTipoInteres;
    		}
    	}
    	
    	return null;
    }
}