package com.prestamossowad.dominio;

public class DetalleSolicitudPrestamo {
    private int id;
    private boolean prestamista;
    private Usuario objUsuario;
    private SolicitudPrestamo objSolicitudPrestamo;

    public DetalleSolicitudPrestamo() {
        this.id = 0;
        this.prestamista = false;
        this.objUsuario = new Usuario();
    }

    public DetalleSolicitudPrestamo(int id, boolean prestamista, Usuario objUsuario, SolicitudPrestamo objSolicitudPrestamo) {
        this.id = id;
        this.prestamista = prestamista;
        this.objUsuario = objUsuario;
        this.objSolicitudPrestamo = objSolicitudPrestamo;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isPrestamista() {
        return prestamista;
    }

    public void setPrestamista(boolean prestamista) {
        this.prestamista = prestamista;
    }

    public Usuario getObjUsuario() {
        return objUsuario;
    }

    public void setObjUsuario(Usuario objUsuario) {
        this.objUsuario = objUsuario;
    }

    public SolicitudPrestamo getObjSolicitudPrestamo() {
        return objSolicitudPrestamo;
    }

    public void setObjSolicitudPrestamo(SolicitudPrestamo objSolicitudPrestamo) {
        this.objSolicitudPrestamo = objSolicitudPrestamo;
    }
}