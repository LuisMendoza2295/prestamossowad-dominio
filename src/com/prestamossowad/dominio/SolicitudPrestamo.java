package com.prestamossowad.dominio;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import com.prestamossowad.dominio.util.DateUtil;

public class SolicitudPrestamo {

    private int id;
    private BigDecimal monto;
    private boolean aprobadoPrestamista;
    private boolean aprobadoPrestatario;
    private int cuotas;
    private boolean estado;
    private Date fechaRegistro;
    private Date fechaModificacion;
    private Moneda objMoneda;
    private TasaInteres objTasaInteres;
    private Usuario objUsuarioPrestamista;
    private Usuario objUsuarioPrestatario;
    private TipoPago[] lstTiposPago;
    private Usuario objUsuarioModificacion;

    public SolicitudPrestamo() {
        this.id = 0;
        this.monto = new BigDecimal(BigInteger.ZERO);
        this.aprobadoPrestamista = false;
        this.aprobadoPrestatario = false;
        this.cuotas = 0;
        this.estado = true;
        this.fechaRegistro = new Date();
        this.fechaModificacion = new Date();
        this.objMoneda = new Moneda();
        this.objTasaInteres = new TasaInteres();
        this.objUsuarioPrestamista = new Usuario();
        this.objUsuarioPrestatario = new Usuario();
        this.lstTiposPago = new TipoPago[0];
        this.objUsuarioModificacion = new Usuario();
        this.fechaModificacion = DateUtil.getFecha(this.fechaModificacion);
    }

    public SolicitudPrestamo(int id, BigDecimal monto, boolean aprobadoPrestamista, boolean aprobadoPrestatario, int cuotas, boolean estado, Date fechaRegistro, Date fechaModificacion, Moneda objMoneda, TasaInteres objTasaInteres, Usuario objUsuarioPrestamista, Usuario objUsuarioPrestatario, List<TipoPago> lstTiposPago, Usuario objUsuarioModificiacion) {
        this.id = id;
        this.monto = monto;
        this.aprobadoPrestamista = aprobadoPrestamista;
        this.aprobadoPrestatario = aprobadoPrestatario;
        this.cuotas = cuotas;
        this.estado = estado;
        this.fechaRegistro = fechaRegistro;
        this.fechaModificacion = fechaModificacion;
        this.objMoneda = objMoneda;
        this.objTasaInteres = objTasaInteres;
        this.objUsuarioPrestamista = objUsuarioPrestamista;
        this.objUsuarioPrestatario = objUsuarioPrestatario;
        this.lstTiposPago = new TipoPago[0];
        this.objUsuarioModificacion = objUsuarioModificiacion;
        this.setLstTiposPago(lstTiposPago);
        this.fechaModificacion = DateUtil.getFecha(this.fechaModificacion);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public BigDecimal getMonto() {
        return monto;
    }

    public void setMonto(BigDecimal monto) {
        this.monto = monto;
    }

    public boolean isAprobadoPrestamista() {
        return aprobadoPrestamista;
    }

    public void setAprobadoPrestamista(boolean aprobadoPrestamista) {
        this.aprobadoPrestamista = aprobadoPrestamista;
    }

    public boolean isAprobadoPrestatario() {
        return aprobadoPrestatario;
    }

    public void setAprobadoPrestatario(boolean aprobadoPrestatario) {
        this.aprobadoPrestatario = aprobadoPrestatario;
    }

    public int getCuotas() {
        return cuotas;
    }

    public void setCuotas(int cuotas) {
        this.cuotas = cuotas;
    }

    public boolean isEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }

    public Date getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(Date fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public Date getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaModificacion(Date fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    public Moneda getObjMoneda() {
        return objMoneda;
    }

    public void setObjMoneda(Moneda objMoneda) {
        this.objMoneda = objMoneda;
    }

    public TasaInteres getObjTasaInteres() {
        return objTasaInteres;
    }

    public void setObjTasaInteres(TasaInteres objTasaInteres) {
        this.objTasaInteres = objTasaInteres;
    }

    public Usuario getObjUsuarioPrestamista() {
        return objUsuarioPrestamista;
    }

    public void setObjUsuarioPrestamista(Usuario objUsuarioPrestamista) {
        this.objUsuarioPrestamista = objUsuarioPrestamista;
    }

    public Usuario getObjUsuarioPrestatario() {
        return objUsuarioPrestatario;
    }

    public void setObjUsuarioPrestatario(Usuario objUsuarioPrestatario) {
        this.objUsuarioPrestatario = objUsuarioPrestatario;
    }

    public TipoPago[] getLstTiposPago() {
        return lstTiposPago;
    }

    public Usuario getObjUsuarioModificacion() {
        return objUsuarioModificacion;
    }

    public void setObjUsuarioModificacion(Usuario objUsuarioModificacion) {
        this.objUsuarioModificacion = objUsuarioModificacion;
    }

    public Prestamo crearPrestamo() {
        Prestamo objPrestamo = new Prestamo(
                0,
                new Date(),
                new Date(),
                this,
                false,
                true);
        objPrestamo.generarCuotas();
        
        return objPrestamo;
    }
    
    public void addTipoPago(TipoPago objTipoPago){
    	this.lstTiposPago = Arrays.copyOf(this.lstTiposPago, this.lstTiposPago.length + 1);
    	this.lstTiposPago[this.lstTiposPago.length - 1] = objTipoPago;
    }
    
    public void setLstTiposPago(List<TipoPago> lstTiposPago){
    	for(TipoPago objTipoPago : lstTiposPago){
    		this.addTipoPago(objTipoPago);
    	}
    }
}
