package com.prestamossowad.dominio;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import com.prestamossowad.dominio.util.BigDecimalUtil;
import com.prestamossowad.dominio.util.DateUtil;

public class Prestamo {

    private int id;
    private BigDecimal tem;
    private Date fechaRegistro;
    private Date fechaModificacion;
    private SolicitudPrestamo objSolicitudPrestamo;
    private boolean pagado;
    private boolean estado;
    private Cuota[] lstCuotas;

    public Prestamo() {
        this.id = 0;
        this.tem = new BigDecimal(BigInteger.ZERO);
        this.fechaRegistro = new Date();
        this.fechaModificacion = new Date();
        this.objSolicitudPrestamo = new SolicitudPrestamo();
        this.pagado = false;
        this.estado = false;
        this.lstCuotas = new Cuota[0];
    }

    public Prestamo(int id, Date fechaRegistro, Date fechaModificacion, SolicitudPrestamo objSolicitudPrestamo, boolean pagado, boolean estado) {
        this.id = id;
        this.fechaRegistro = fechaRegistro;
        this.fechaModificacion = fechaModificacion;
        this.objSolicitudPrestamo = objSolicitudPrestamo;
        this.pagado = pagado;
        this.estado = estado;
        this.tem = this.calcularTEM();
        this.generarCuotas();
        this.fechaModificacion = DateUtil.getFecha(this.fechaModificacion);
    }
    
    public Prestamo(int id, BigDecimal tem, Date fechaRegistro, Date fechaModificacion, SolicitudPrestamo objSolicitudPrestamo, boolean pagado, boolean estado, List<Cuota> lstCuotas){
    	this.id = id;
    	this.tem = tem;
    	this.fechaRegistro = fechaRegistro;
    	this.fechaModificacion = fechaModificacion;
    	this.objSolicitudPrestamo = objSolicitudPrestamo;
    	this.pagado = pagado;
    	this.estado = estado;
    	this.lstCuotas = new Cuota[0];
    	this.setLstCuotas(lstCuotas);
    	this.fechaModificacion = DateUtil.getFecha(this.fechaModificacion);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public BigDecimal getTem() {
        return tem;
    }

    public void setTem(BigDecimal tem) {
        this.tem = tem;
    }

    public Date getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(Date fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public Date getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaModificacion(Date fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    public SolicitudPrestamo getObjSolicitudPrestamo() {
        return objSolicitudPrestamo;
    }

    public void setObjSolicitudPrestamo(SolicitudPrestamo objSolicitudPrestamo) {
        this.objSolicitudPrestamo = objSolicitudPrestamo;
    }

    public boolean isPagado() {
        return pagado;
    }

    public void setPagado(boolean pagado) {
        this.pagado = pagado;
    }

    public boolean isEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }

    public Cuota[] getLstCuotas() {
        return lstCuotas;
    }

    public void generarCuotas() {
        List<Cuota> lstCuotasGeneradas = new ArrayList<>();
        
        this.tem = this.calcularTEM();
        
        BigDecimal temPotencia = (BigDecimal.valueOf(1).add(this.tem)).pow(this.objSolicitudPrestamo.getCuotas());
        
        BigDecimal numerador = this.tem.multiply(temPotencia);
        BigDecimal denominador = temPotencia.subtract(BigDecimal.valueOf(1));
        
        BigDecimal cuota = this.objSolicitudPrestamo.getMonto().multiply((numerador.divide(denominador, 4, RoundingMode.HALF_UP)));
        cuota = BigDecimalUtil.round(cuota, 4);
        
        BigDecimal interes = new BigDecimal(BigInteger.ZERO);
        BigDecimal amortizacion = new BigDecimal(BigInteger.ZERO);
        BigDecimal saldo = new BigDecimal(BigInteger.ZERO);
        saldo = saldo.add(this.objSolicitudPrestamo.getMonto());
        Calendar fechaInicio = new GregorianCalendar();
        fechaInicio.setTime(new Date());
        Calendar fechaFin = new GregorianCalendar();
        for(int i = 0; i < this.objSolicitudPrestamo.getCuotas(); i++){
            interes = saldo.multiply(this.tem);
            amortizacion = cuota.subtract(interes);
            saldo = saldo.subtract(amortizacion);
            
            interes = BigDecimalUtil.round(interes, 4);
            amortizacion = BigDecimalUtil.round(amortizacion, 4);
            saldo = BigDecimalUtil.round(saldo, 4);
            
            fechaFin.setTime(fechaInicio.getTime());
            fechaFin.add(Calendar.DAY_OF_MONTH, this.objSolicitudPrestamo.getObjTasaInteres().getObjTipoInteres().getDiasPlazos());
            Cuota objCuota = new Cuota(0, amortizacion, interes, cuota, new BigDecimal(BigInteger.ZERO), fechaInicio.getTime(), fechaFin.getTime(), new Date(), null, false);
            lstCuotasGeneradas.add(objCuota);
            fechaInicio.setTime(fechaFin.getTime());
        }
        
        Cuota[] lstCuotasArray = new Cuota[0];
        for(Cuota objCuota : lstCuotasGeneradas){
        	lstCuotasArray = Arrays.copyOf(lstCuotasArray, lstCuotasArray.length + 1);
        	lstCuotasArray[lstCuotasArray.length - 1] = objCuota;
        }
        
        this.lstCuotas = lstCuotasArray;
    }

    public BigDecimal calcularTEM() {
        BigDecimal temValue = new BigDecimal(BigInteger.ZERO);

        try {
            BigDecimal potencia = BigDecimal.valueOf(this.objSolicitudPrestamo.getObjTasaInteres().getObjTipoInteres().getDiasPlazos()).divide(BigDecimal.valueOf(360), 4, RoundingMode.HALF_UP);
            BigDecimal base = BigDecimal.valueOf(1).add(this.objSolicitudPrestamo.getObjTasaInteres().getTea());

            temValue = BigDecimal.valueOf(Math.pow(base.doubleValue(), potencia.doubleValue())).subtract(BigDecimal.valueOf(1));
        }catch(Exception e){
            e.printStackTrace();
        }
        
        return temValue;
    }
    
    public void addCuota(Cuota objCuota){
    	this.lstCuotas = Arrays.copyOf(this.lstCuotas, this.lstCuotas.length + 1);
    	this.lstCuotas[this.lstCuotas.length - 1] = objCuota;
    }
    
    public void setLstCuotas(List<Cuota> lstCuotas){
    	for(Cuota objCuota : lstCuotas){
    		this.addCuota(objCuota);
    	}
    }
}